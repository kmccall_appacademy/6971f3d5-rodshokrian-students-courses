require 'course'
class Student

  def initialize(f, l)
    @first_name = f
    @last_name = l
    @course_list = []
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def courses
    @course_list
  end

  def enroll(course)
    return if courses.include?(course)
    raise 'This course\'s time block conflicts with a currently enrolled course' if self.courses.any? {|enrolled_course| course.conflicts_with?(enrolled_course)}
    course.add_student(self)
    courses << course
#take a 'Course' object, add it to the student's list of courses, and update the 'Courses'
#list of enrolled students
#Should also ignore attempts to re-enroll students
  end

  def course_load
    course_hash = Hash.new(0)
    courses.each {|i| course_hash[i.department] += i.credits}
    course_hash
#should return a hash of departments (keys) and # of credits (values) the student is taking in each department
  end

end
